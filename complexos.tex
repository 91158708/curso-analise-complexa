\section{Os Números Complexos}
\label{sc:os_numeros_complexos}

Seja
\[
  \C := \{ a + ib : a, b \in \R \}
\]
a álgebra sobre $\R$ definida por $i^2 = - 1$.
Se $z = a + ib$, denotemos $a = \Re z$ e $b = \Im z$ e chamamo-los a parte \emph{real} respectivamente \emph{imaginária}.
Temos
\[
  z^2 + w^2 = (z + iw)(z - iw).
\]
Logo, para $z = a$ e $w = b$ em $\R$, temos
\[
  \frac{1}{a + ib} = \frac{a - ib}{a^2 + b^2} = \frac{a}{a^2 + b^2} - i \frac{b}{a^2 + b^2},
  \quad \tag{$*$}
\]
isto é, $\C$ é um corpo.

\subsection{Conjugação e Norma}
\label{ssc:conjugacao_e_norma}

Seja a aplicação $\sigma = \bar \cdot$ dada por
\[
  \overline{a + ib} = a - ib;
\]
é um \emph{automorfismo}, isto é, envia $\C$ a si mesmo e respeita as operações $+$ e $\cdot$.
Seja a norma de $\C$ dada por
\[
  \norm{a + ib} := \sqrt[2]{a^2 + b^2};
\]
é um \emph{valor absoluto}, isto é uma aplicação $\norm{\cdot} \from \C \to \R_{\geq 0}$ tal que
\begin{itemize}
  \item (Hausdorff) $\norm{x} = 0 \text{ se, e somente se, } x = 0$,
  \item (Multiplicatividade) $\norm{xy} = \norm{x}\norm{y}$, e
  \item (Desigualdade Triangular) $\norm{x + y} \leq \norm{x} + \norm{y}$.
\end{itemize}
Para demonstrar a desigualdade triangular, observamos
\[
  \norm{z + w}^2
  =
  \norm{z}^2 + 2 \Re  (z \bar w) + \bar w^2
  \leq
  \norm{z}^2 + 2 \norm{z}\norm{w} + \bar w^2
  =
  (\norm{z} + \norm{w})^2.
\]

\begin{obs}
  Temos $\norm{z + w} = \norm{z} + \norm{w}$ se, e tão-somente se, $\norm{z \bar w} = \Re z \bar w$ se, e tão-somente se, $z \bar w \geq 0$.
\end{obs}

O automorfismo é \emph{isométrico}, isto é, $\norm{\bar z} = \norm{z}$.
Temos
\[
  \norm{z}^2 = \overline{z} z
\]
Logo, se $z = a + ib \neq 0$, então $(*)$ é
\[
  \frac{1}{z} = \frac{\bar z}{\norm{z}^2}.
\]
Observamos
\[
  \Re z = \frac{z + \bar z}{2}
  \quad e \quad
  \Im z = \frac{z - \bar z}{2}
\]

\begin{task}
  Demonstra
  \[
    \norm{\norm{z} - \norm{w}} \leq \norm{z - w}.
  \]
\end{task}

\subsection{Projeção estereográfica}
\label{ssc:projecao_sester}

Queremos acrescentar um ponto infinito a $\C$:
Seja
\[
  S = \{ (x_1, x_2, x_3) \in \R^3 : x_1^2 + x_2^2 + x_3^2 = 1 \}
\]
a esfera unitária em $\R^3$.
Seja $N = (0, 0, 1)$ o pólo norte e identifique
\[
  \C = \{ (x_1, x_2, x_3) \in \R^3 : x_3 = 0 \}.
\]
Para todo $z \in \C$, considere a linha que passa por $z$ e $N \in S$ e que encontra $S$ em um único ponto $Z$:
\begin{itemize}
  \item Se $\norm{z} < 1$, então $Z$ está no hemisfério sul;
  \item Se $\norm{z} > 1$, então $Z$ está no hemisfério norte;
  \item Se $\norm{z} = 1$, então $z = Z$ está no equador.
\end{itemize}
Se $\norm{z} \to \infty$, então $Z$ aproxima $N$;
logo $N$ será identificado com $\infty$ e
\[
  S = \C \cup \{ \infty \}.
\]
A linha através de $N$ e $z$ é dada pela pontos da forma
\[
  z + t (N - z) = t N + (1-t)z \quad \text{ para } t \in ]\infty, \infty[.
\]
ou, usando o valor de $N$ e $z = (x, y, 0)$,
\[
  ((1-t)x, (1-t)y, t) \quad \text{ para } t \in ]\infty, \infty[.
  \tag{$*$}
\]
Para encontrar o valor de $t$ para $Z$, observamos
\[
  1 = \norm{Z} = (1-t)^2 x^2 + (1-t)^2 y^2 + t^2
  = (1-t)^2 \norm{z}^2 + t^2,
\]
logo
\[
  1 - t^2 = (1-t)^2 \norm{z}^2.
\]
Como $t \neq 1$, temos, dividindo por $1 - t$,
\[
  t = \frac{\norm{z}^2 - 1}{\norm{z}^2 + 1}.
\]
Logo, para $(x_1, x_2, x_3) = Z = t N + (1-t)z$ com $N = (0, 0, 1)$ e $z = (x, y, 0)$,
\[
  x_1 = \frac{2x}{\norm{z}^2 + 1},
  x_2 = \frac{2y}{\norm{z}^2 + 1},
  \text{ e }
  x_3 = \frac{\norm{z}^2 - 1}{\norm{z}^2 + 1}
\]
ou
\[
  \label{eq:formula-Z}
  Z = (x_1, x_2, x_3)
  \text{ onde }
  x_1 = \frac{z + \bar z}{\norm{z}^2 + 1},
  x_2 = \frac{-i(-z - \bar z)}{\norm{z}^2 + 1},
  \text{ e }
  x_3 = \frac{\norm{z}^2 - 1}{\norm{z}^2 + 1}.
\]
Vice-versa, dado $Z = (x_1, x_2, x_3)$, para encontrar $z = (x, y, 0)$, por $(*)$, $1 - t = 1 - x_3$, logo
\[
  \label{eq:formula-z}
  z = \frac{x_1 + ix_2 }{1 - x_3}.
\]

Definhamos uma distância sobre $S$ por
\[
  \dist(Z, Z')
  :=
  \sqrt[2]{(x_1 - x_1')^2 + (x_2 - x_2')^2 + (x_3 - x_3')^2}
\]
Como $\norm{Z} = \norm{Z'} = 1$, obtemos
\[
  \dist(Z, Z')
  =
  2 - 2(x_1x_1' + x_2x_2' + x_3 x_3').
\]
Por \eqref{eq:formula-Z},
\[
  \dist(Z, Z')
  =
  \frac{2 \norm{z - z'}}{\sqrt[2]{(1 + \norm{z}^2)(1 + \norm{z'}^2)}}
\]
Semelhantemente,
\[
  \dist(z, \infty)
  =
  \frac{2}{\sqrt[2]{1 + \norm{z}^2}}.
\]

% ex: set spelllang=pt:
