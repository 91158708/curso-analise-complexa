\section{Espaços Métricos}
\label{sc:espacos_metricos}

\begin{defn*}
  Seja $X$ um conjunto.
  Uma aplicação $\dst \from X \x X \to [0, \infty[$ é uma \emph{distância} se
  \begin{itemize}
    \item
      $d(x,y) = 0$ se, e somente se, $x = y$,
    \item
      $d(x,y) = d(y,x)$, e
    \item
      $d(x,z) \leq d(x,y) + d(y,z)$.
  \end{itemize}
  Um \emph{espaço métrico} é um par $(X, d)$ de um conjunto $X$ e uma distância $d$ como acima.

  Uma aplicação $f$ entre espaços métricos é \emph{uniformemente contínua} se
  \[
    \text{para todo } \epsilon > 0,
    \text{ existe } \delta > 0 \text{ tal que }
    \dst(x,y) < \delta \text{ implica }
    \dst(f(x), f(y)) < \epsilon.
  \]
\end{defn*}

\begin{defn*}[Bolas]
  Para $x$ em $X$ e $r > 0$, defina a \emph{bola aberta} respectivamente \emph{fechada} com \emph{raio} $r$ e \emph{centro} $x$ por
  \[
    \Ball(x; r) = \{ y \in X : d(x, y) < r \}
    \quad \text{ e } \quad
    \bar\Ball(x; r) = \{ y \in X : d(x, y) \leq r \}.
  \]
\end{defn*}

\begin{ex}
  \hfill
  \begin{itemize}
    \item
      Seja $X = \R$ ou $\C$ e define $d(x, y) = \norm{x - y}$.
      O espaço métrico será para nós o exemplo prototípico.
    \item Se $X$ é um espaço métrico e $Y \subseteq X$, então $Y$ é um espaço métrico.
    \item Em vez da norma euclidiana sobre $\R \times \R$, podemos usar a soma $\Norm{x, y} = \norm{x} + \norm{y}$ com a métrica induzida.
    \item Ou podemos usar o máximo $\Norm{x, y} = \max \{\norm{x}, \norm{y}\}$ com a métrica induzida.
    \item A norma trivial $\norm{0} = 0$ $\norm{x} = 1$ para todo $x \neq 0$.
    \item Sobre $\R^n$, podemos usar
      \[
        \Norm{x_1, \ldots, x_n}
        =
        \sqrt[2]{x_1^2 + \cdots + x_n^2}.
      \]
    \item Seja $S$ um conjunto e $B(S)$ todas as funções limitadas, isto é, tal que
      \[
        \Norm{f}_\infty
        :=
        \sup \{ \norm{f(x)} : x \in S \}
        < \infty
      \]
      com a norma induzida.
  \end{itemize}
\end{ex}

  \subsection{Topologia}
  \label{ssc:topologia}

  \begin{defn}
    Um conjunto $G \subseteq X$ é \emph{aberto} se para todo $g$ em $G$ existe uma bola $B \subseteq G$.
  \end{defn}

  \begin{prop}
    \label{prop:conjuntos-abertos}
    \hfill
    \begin{itemize}
      \item Os conjuntos $X$ e $\emptyset$ são abertos.
      \item Se $G_1$, \ldots, $G_n$ são abertos, então $G_1 \cap \ldots \cap G_n$ é aberto.
      \item Se $G_i$ para $i$ em $I$ são abertos, então $\bigcup G_i$ é aberto.
    \end{itemize}
  \end{prop}

  \begin{defn}
    Um conjunto $F \subseteq X$ é \emph{fechado} se $X - F$ é aberto.
  \end{defn}

  \begin{prop}[O complemento a {\cref{prop:conjuntos-abertos}}]
    \hfill
    \begin{itemize}
      \item Os conjuntos $X$ e $\emptyset$ são fechados.
      \item Se $G_1$, \ldots, $G_n$ são fechados, então $G_1 \cup \ldots \cup G_n$ é aberto.
      \item Se $G_i$ para $i$ em $I$ são fechados, então $\bigcap G_i$ é aberto.
    \end{itemize}
  \end{prop}

  \begin{defn}
    Seja $A \subseteq X$.
    O \emph{interior} $A^\circ$ de $A$ é
    \[
      A^\circ = \bigcup \{ B \subseteq A : B \text{ aberto } \}
    \]
    e o \emph{fecho} $A^-$ de $A$ é
    \[
      A^- = \bigcap \{ B \supseteq A : B \text{ fechado } \}.
    \]
    O bordo $\partial A$ é
    \[
      \partial A = A^- - A^\circ.
    \]
  \end{defn}

  Possivelmente, $A^\circ = \emptyset$ e $A^- = X$.
  O interior é aberto e o fecho fechado.

  \begin{prop}
    Se $A, B \subseteq X$, então
    \begin{enumerate}
      \item A é aberto se, e tão-somente se, $A^\circ = A$
      \item A é fechado se, e tão-somente se, $A^- = A$
      \item $A^\circ = X - (X - A)^-$ e $A^- = X - (X - A)^\circ$.
      \item $(A \cup B)^- = A^- \cup B^-$
      \item $x \in A^\circ$ se, e tão-somente se, $A$ contém uma bola em torno de $x$.
      \item $x \in A^-$ se, e tão-somente se, $X - A$ intersecta toda bola em torno de $x$.
    \end{enumerate}
  \end{prop}
  \begin{proof}
    Para (vi), e a implicação $\implies$ seja $x$ em $A^- = X - (X - A)^\circ$, isto é, $x \notin (X - A)^\circ$.
    Logo, por (v), nenhuma bola em torno de $x$ é contida em $X - A$.
    Isto é, para toda bola $B$ em torno de $x$ existe $y$ em $B$ com $y \in A$.

    Para $\impliedby$, seja $x \notin A^- = X - (X - A)^\circ$, isto é, $x \in (X - A)^\circ$.
    Por (v), existe uma bola $B$ em torno de $x$ contida em $X - A$, isto é, tal que $B \cap A = \emptyset$.
  \end{proof}

  \begin{defn}
    Um subconjunto $A \subseteq X$ é \emph{denso} se $A^- = X$.
  \end{defn}

  Por exemplo, $\Q$ é denso em $\R$

  \subsection{Conexidade}
  \label{ssc:conexidade}

  \begin{defn}
    Um espaço métrico $X$ é \emph{conexo} se os únicos subconjuntos que são no mesmo tempo fechados e abertos são $X$ e $\emptyset$.
    Equivalentemente, se existem subconjuntos não-vazios $Y$ e $Z$ tais que $X = Y \dot\cup Z$ é a sua união disjunta.
  \end{defn}

  \begin{ex}
    Se $X = \Ball(x') \dot\cup \bar \Ball(x'')$ é a união disjunto de uma bola aberta e outra bola fechada, então qualquer destas bolas é aberto e fechado em $X$;
    isto é, $X$ é \emph{desconexo}.
  \end{ex}

  \begin{prop}
    Um subconjunto $X \subseteq \R$ é conexo se, e tão-somente se, $X$ é um intervalo.
  \end{prop}
  \begin{proof}
    Seja $X = [a, b]$ um intervalo.
    Seja $A \subset X$ um próprio subconjunto aberto que contenha $a$.

    Basta mostrar que $A$ não é aberto.

    Demonstremo-lo:
    Seja $\epsilon > 0$ tal que $[a, a + \epsilon[ \subseteq A$.
    Seja
    \[
      r = \sup \{ \epsilon : [a, a + \epsilon[ \subseteq A \}.
    \]
    Pela definição do supremum $[a, a+r[ \subseteq A$.

    Pela definição de $r$, contudo $a + r \notin A$:
    Caso contrário, por $A$ ser aberto, existira $\delta > 0$ tal que $[a+r, a+r+\delta[ \subseteq A$, logo $[a, a+r+\delta[ \subseteq A$, em contradição à definição de $r$.

    Se $A$ fosse também fechado, $a + r \in A$, contradição

    A demonstração para outros tipos de intervalos são semelhantes.
    A implicação inversa é um exercício.
  \end{proof}

  \subsection{Funções Uniformemente Contínuas}
  \label{ssc:funcoes_uniformemente_continuas}


  Como as aplicações que respeitam a estrutura entre espaços topológicos são as aplicações \emph{contínuas} e entre espaços vetoriais as aplicações \emph{lineares}, aqui, entre espaços métricos, supomos todas as aplicações \emph{uniformemente contínuas}.

  Se $\norm{\cdot}$ é uma norma sobre um anel, então
  \[
    d(x,y) := \norm{x-y}
  \]
  é uma distância.

  \begin{defn*}
    Uma sequência $(x_n)$ em um espaço métrico é dita de \emph{Cauchy} se para todo $\epsilon > 0$, existe $N$ tal que $\dst(x_n, x_m) < \epsilon$ para todos os $n,m > N$.
    Um espaço métrico é \emph{completo} se toda sequência de Cauchy converge.
  \end{defn*}

  Em particular,  toda sequência que converge é Cauchy.

    \subsection{Compacidade}
    \label{ssc:compacidade}

    \begin{defn}
      Um corpo $\K$ é \emph{localmente compacto} se para todo $x$ em $\K$ existe uma vizinhança $V \owns x$ tal que o seu fechamento é compacto.
    \end{defn}

    Por exemplo, $\K = \R$, $\C$ e as extensões finitas de $\Q_p$ e $\F_p((t))$ são localmente compactos (e de fato constituem todos tais corpos).
    Já vimos que $\Z_p$, a bola unitária de $\Q_p$, é compacto;
    logo $\Q_p$ é localmente compacto (e semelhantemente $\F_p((t))$).
    Para ver que $\R$ (e logo $\C$) é localmente compacto, basta por \cref{thm:metrico-sequencialmente-compacto} mostrar que todo subconjunto limitado é sequencialmente compacto, isto é, que toda sequência limitada tem uma subsequência convergente.

    \begin{lem}
      \label{lem:subsequencia-monotona}
      Toda sequência infinita em $\R$ tem uma subsequência monótona.
    \end{lem}
    \begin{proof}
      Um índice $n$ é um \emph{pico} se $x_n > x_{n+1}$, $x_{n+2}$, \ldots.
      Se tem uma infinitude de picos $n_1$, $n_2$, \ldots, então $x_{n_1}$, $x_{n_2}$, \ldots, é uma subsequência monotonamente decrescente.
      Caso contrário, seja $N$ o último pico.
      Isto é, para todo $n > N$ existe $m > n$ com $x_m \geq x_n$.
      Seja $n_1 = N + 1$;
      logo, existe $n_2 > n_1$ tal que $x_{n_2} \geq x_{n_1}$;
      semelhantemente para $n_2$ no lugar de $n_1$, e assim por diante se constrói uma subsequência $(x_{n_k})$ monotonamente crescente.
    \end{proof}

    \begin{lem}
      \label{lem:monotono-limitada-convergente}
      Toda sequência monótona e limitada converge;
      caso crescente ao seu supremum, caso decrescente ao seu infimum.
    \end{lem}
    \begin{proof}
      Seja $(x_n)$ monotonamente crescente e limitada.
      Seja $s = \sup \{ x_n \}$ e $\epsilon > 0$.
      Logo existe $N$ tal que $\norm{s - x_N} < \epsilon$.
      Pela monotonia, a fortiori $\norm{s - x_n} < \epsilon$ para todo $n > N$.
      Isto é, $x_n \to s$.

      Analogamente para uma sequência monotonamente decrescente.
    \end{proof}

    \begin{cor}
      Seja $(x_n)$ é uma sequência em $\R$.
      Se $(x_n)$ é limitado, então tem uma subsequência convergente.
    \end{cor}
    \begin{proof}
      Por \cref{lem:subsequencia-monotona} e \cref{lem:monotono-limitada-convergente}.
    \end{proof}

    % TODO Mostra que $\R$ é localmente compacto.
    Se a bola unitária de $\K$ é compacta, então $\K$ é localmente compacto.

    \begin{thm}[de Banach-Alaoglu]
      Seja $\K$ um corpo topológico e $V$ um espaço normado sobre $\K$.
      Se $\K$ é localmente compacto, então a bola unitária é compacta para a topologia fraca$^*$.
    \end{thm}
    \begin{proof}
      Seja $B$ a bola unitária de $V$ e Seja $B^*$ a bola unitária de $V*$.
      Seja $b$ a bola unitária de $\K$.
      Logo
      \[
        B^* = \{ f \colon B \to b : \text{ linear } \}
        \subseteq
        b^B
      \]
      Pela Teorema de Tychonoff, O produto $b^B$ de compactos $b$ é compacto para a topologia do produto.
      Por definição a topologia do produto é igual à topologia fraca$^*$.
    \end{proof}

    Um subconjunto de um espaço topológico é \emph{denso} se intersecta todo subconjunto aberto.
    Um espaço topológico é \emph{separável} se tem um subconjunto enumerável denso.
    Um espaço topológico é \emph{sequencialmente compacto} se toda sequência tem uma subsequência convergente.

    \begin{ex}
      Não todo espaço compacto é sequencialmente compacto:
      Por exemplo, a bola unitária do dual $\fct^b(\N)^*$ é compacto para a topologia fraca$^*$ pelo Teorema de Alaoglu.
      Porém, a sequência $([f \mapsto f(n)] : n \in \N)$ não tem nenhuma subsequência convergente.
    \end{ex}

    \begin{lem}
      Se $X$ é separável, então existe uma métrica cujas bolas geram a topologia fraca$^*$.
    \end{lem}
    \begin{proof}
      Para um subconjunto $ \{ x_n \} $ denso, define
      \[
        \dist(x, y)
        :=
        \sum_{n \in \N} 2^{-n} \norm{x-y(x_n)}/ 1 + \norm{x-y(x_n)}.
      \]
    \end{proof}

    \begin{thm}
      \label{thm:metrico-sequencialmente-compacto}
      Se $X$ é um espaço métrico, então $X$ é sequencialmente compacto se, e tão-somente se, $X$ é compacto.
    \end{thm}
    \begin{proof}
      Mostremos a implicação $\impliedby$ por contraposição, isto é, se não é sequencialmente compacto, então não é compacto:
      Seja $(x_n)$ tal que nenhuma subsequência convirja, isto é, para todo $x$ em $X$ existe $\epsilon(x) > 0$ tal que $\Ball(x, \epsilon(x)) \cap \{ x_n \}$ é finita.
      Se a cobertura $\{\Ball(x, \epsilon(x)) : x \in X\}$ tivesse um refinamento finito, então $(x_n)$ seria finita, em particular, converge;
      em contradição ao que nenhuma subsequência converge.
      Logo $X$ não é compacto.

      Mostremos a implicação $\implies$ em três passos:
      \begin{enumerate}
        \item Para toda cobertura $ \{ U_i \} $ existe $\epsilon > 0$ tal que para todo $x$ em $X$ existe $i$ tal que $\Ball(x, \epsilon) \subseteq U_i$.
        \item Para todo $\epsilon > 0$ existe uma cobertura finita de $X$ por bolas $\Ball(x, \epsilon)$.
        \item  O espaço topológico $X$ é compacto.
      \end{enumerate}
      \begin{enumerate}[{Ad} (i):]
        \item
          Por contraposição:
          Se existe uma cobertura $ \{ U_i \} $ tal que para todo $n$ existe $x_n$ em $X$ tal que $\Ball(x_n, 1/n)$ não é contida em nenhum $U_i$.
          Então $(x_n)$ tem nenhuma subsequência convergente.
        \item
          Por contraposição:
          Se existe $\epsilon > 0$ tal que para todo $n$ existe $x_{n+1} \not\in \Ball(x_1, \epsilon) \cup \ldots \cup \Ball(x_n, \epsilon)$, então $(x_n)$ tem nenhuma subsequência convergente.
        \item
          Seja $ \{ U_i \} $ uma cobertura de $X$.
          Seja $\epsilon > 0$ dado por (i) e $x_1$, \ldots, $x_n$ por (ii).
          Logo o número finito $U_{i_1}$, \ldots, $U_{i_n}$ tais que $x_1$, \ldots, $x_n$ é em $U_{i_1} \cup \ldots \cup U_{i_n}$ cobre $X$.
          \qedhere
      \end{enumerate}
    \end{proof}

    \subsection{Corpos Locais}
    \label{ssc:corpos_locais}

    \begin{defn*}
      A função $\norm{\cdot} \from \K \to [0, \infty[$ é um \emph{valor absoluto não-arquimediano} se ela satisfaz
      \begin{itemize}
        \item $\norm{x} = 0$ se, e somente se, $x = 0$,
        \item $\norm{xy} = \norm{x} \norm{y}$, e
        \item $\norm{x + y} \leq \max \{ \norm{x}, \norm{y} \}$.
      \end{itemize}
    \end{defn*}

    Em comparação a um valor absoluto geral, a desigualdade triangular satisfeita por um valor absoluto não-arquimediano é mais forte, a \emph{desigualdade triangular mais forte} ou \emph{ultramétrica}.

    \begin{defn*}
      A função $v \from \K \to ]-\infty, \infty]$ é uma \emph{valoração} se ela satisfaz
      \begin{itemize}
        \item $v x = - \infty$ se, e somente se, $x = 0$,
        \item $v (xy) = v x + v y$, e
        \item $v (x + y) \geq \min \{ v x, v y \}$
      \end{itemize}
    \end{defn*}

    Se $\norm{\cdot}$ é um valor absoluto não-arquimediano, então para qualquer $c > 1$, a função $v:= \log_c \norm{\cdot}$ é uma valoração;
    e, vice-versa, se $v$ é uma valoração, então para qualquer $c < 1$ a função $\norm{\cdot} := c^{v(\cdot)}$ é um valor absoluto não-arquimediano.

    \begin{ex*}
      \hfill
      \begin{itemize}
        \item O corpo $\K = \Q_p$ com valoração $v(x) = n$ se $x = p^n \frac{x'}{x''}$ com $p \nmid x', x''$.
        \item O corpo $\K = \F_p((t))$ com valoração $v(x) = n$ se $x = a_n t^n + \dotsb$ com $a_n \neq 0$.
      \end{itemize}
    \end{ex*}

    Poderíamos concluir pelos exemplos que, enquanto o valor absoluto $\norm{\cdot}$ necessita um contorno da definição mas é mais próximo do intuito (adquirido do cálculo real), a valoração é mais próxima da definição, ao custo do intuito.

    \begin{prop}[Propriedades não-arquimedianas]
      \hfill
      \begin{itemize}
        \item
          Se $x = x_1 + \dotsb + x_n$ e existe $i_0$ em $ \{ 1, \ldots, n \} $ tal que $\norm{x_{i_0}} > \norm{x_i}$ para $i \neq i_0$,
          então $\norm{x} = \norm{x_{i_0}}$;
        \item
          Se $x_n \to x$, então $\norm{x_n} = \norm{x}$ para $n$ suficientemente grande.
        \item
          Se $\K$ é completo, então $\sum_n x_n$ converge se, e somente se, $x_n \to 0$.
      \end{itemize}
    \end{prop}
    \begin{proof}
      \hfill
      \begin{itemize}
        \item
          Observa que (o caso $n = 2$) se $x, y$ em $\K$ e $\norm{x} > \norm{y}$, então $\norm{x + y} \leq \norm{x}$ e
          \[
            \norm{x}
            =
            \norm{x + y-y}
            \leq
            \max \{ \norm{x + y}, \norm{y} \},
          \]
          então, como $\norm{y} < \norm{x}$, segue $\norm{x} \leq \norm{x + y}$.
          Concluímos $\norm{x} = \norm{x + y}$.
        \item
          Se $\epsilon < \norm{x}$, então $\norm{x-x_n} < \epsilon$ para $n$ suficientemente grande;
          logo, pelo primeiro item, $\norm{x_n} = \norm{x}$.
        \item
          Pela desigualdade triangular mais forte,
          \[
            \norm{x_m + \dotsb + x_M} \leq \max \{ \norm{x_m}, \ldots, \norm{x_M} \} \to 0.
            \qedhere
          \]
      \end{itemize}
    \end{proof}

    Seja $\K$ um corpo com valor absoluto não-arquimediano.
    \begin{itemize}
      \item O \emph{anel dos inteiros} $\o_\K := \{ x \text{ em } \K \text{ tal que } \norm{x} \leq 1 \}$,
      \item o \emph{ideal máximo} $\m_\K := \{ x \text{ em } \K \text{ tal que } \norm{x} < 1 \}$, e
      \item o \emph{corpo residual} $\k_\K := \o_\K / \m_\K$.
    \end{itemize}

    Notamos que $\norm{x} = 1$ se, e somente se, $\norm{x^{-1}} = 1$, e por isso a união disjunta $\o_\K = \o_\K^* \cup \m_\K$;
    isto é, $\m_\K$ é o ideal máximo local.

    A valoração é \emph{discreta} se $v(\K^*)$ é discreta em $\R$;
    se, e somente se, existe um $c$ em $\R$ tal que $v(\K^*) = c \Z$;
    se, e somente se, existe um $\pi_\K$ em $\K$ que gera $\m_\K$.

    Tal $\pi_\K$ é um \emph{uniformizador} de $\K$.

    \begin{ex*}
      \hfill
      \begin{itemize}
        \item
          Se $\K = \Q_p$, então $\o_\K = \Z_p$, $\m_\K = p \Z_p$ e $\k_\K = \F_p$ (e $\pi_\K = p$), e
        \item
          Se $\K = \F_p((t))$, então $\o_\K = \F_p[[t]]$, $\m_\K = t \F_p[[t]]$ e $\k_\K = \F_p$ (e $\pi_\K = t$).
      \end{itemize}
    \end{ex*}

    Um corpo com um valor absoluto $v$ é \emph{local} se
    \begin{itemize}
      \item o espaço métrico induzido é completo, e
      \item se $v$ é discreta, e
      \item o corpo residual $\k_\K$ é finito.
    \end{itemize}

    \begin{ex*}
      Os corpos $\Q_p$ e $\F_p((t))$ são completos.
    \end{ex*}

    \begin{thm*}
      % \label{thm:corpos-locais}
      Vale
      \begin{align}
        \{ \text{ corpos locais } \} & = \{ \text{ extensões finitas de $\Q_p$ } \}\\
                                    & \cup \{ \text{ extensões finitas de $\F_p((t))$ } \}.
      \end{align}
    \end{thm*}

    O seguinte teorema surpreende, porque a partir de uma propriedade inteiramente topológica, a compacidade local, nasce um valor absoluto.
    Este valor absoluto é dado pela \emph{medida de Haar} que existe sobre qualquer grupo topológico localmente compacto.

    \begin{thm*}
      Vale
      \[
        \{ \text{ corpos topológicos localmente compactos } \}
        =
        \{ \R, \C \} \cup \{ \text{ corpos locais } \}.
      \]
    \end{thm*}

% ex: set spelllang=pt:
